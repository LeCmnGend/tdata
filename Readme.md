# To run custom user_dir (tData userdir)

For example of backtesting
```
freqtrade download-data --userdir=tData -c tData/config-user.json  --timeframe 1m --days 452

freqtrade download-data --userdir=tData -c tData/config-user.json  --timeframe 3m --days 452 -p "BTC/USDT"

freqtrade backtesting -c=tData/config.json -s=tScalp --timerange 20221231-20230131 --cache none --userdir tData
```

# Config file

Config-user.json have all of your data, and other people can access and take your money

So keep it safe and do not push in git (here just a empty file for example)

To make it un sync with git, run:

```
git update-index --assume-unchanged config-user.json
```

here is an empty **config-user.json** file:
```
{
	"add_config_files": [
		"configs/config.json",
		"configs/config-StaticPair.json",
	],
	"dry_run": true,
	"dry_run_wallet": 1300,
    "exchange": {
        "name": "binance",
        "key": "",
        "secret": "",
    },
    "telegram": {
        "enabled": true,
        "token": "",
        "chat_id": "",
	}
}
``` 

Save it to tData foler then you can run above backtesting command