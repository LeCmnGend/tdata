# --- Do not remove these libs ---
from freqtrade.strategy import IStrategy, IntParameter, CategoricalParameter, DecimalParameter, RealParameter
from pandas import DataFrame

#-------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------
class tBuySell(IStrategy):
    timeVer = '- 2024.07'

    INTERFACE_VERSION: int = 3

    minimal_roi = {
        "0":     0.1390,
        "600":   0.1050,
        "8000":  0.0890,
        "16000": 0.0065,
    }
    {
        "ignore_buying_expired_candle_after": 1300, #second
    }
    # Optimal stoploss designed for the strategy
    # This attribute will be overridden if the config file contains "stoploss"
    # should not be below a 9% loss
    stoploss = -0.345
    trailing_stop = False

#Hyperopt parameters------------------------------------------------------------------
#-------------------------------------------------------------------------------------
    """
    PASTE OUTPUT FROM HYPEROPT HERE
    Can be overridden for specific sub-strategies (stake currencies) at the bottom.
    """
    
    # hypered params
    buy_params = {
        "bbdelta_close": 0.01889,
        "bbdelta_tail": 0.72235,
        "close_bblower": 0.0127,
        "closedelta_close": 0.00916,
        "buy_pows": 0.9967,     #0.9989 9996                             #smaller is harder
        "buy_bbdelta": 7,
        "buy_closedelta": 17,
        "buy_tail": 25,
    }

    # Sell hyperspace params:
    sell_params = {
        'sell_fisher': 0.39075,
        'sell_bbmiddle_close': 0.99754,
        'sell_pows': 1.0024, #1.00071                                 #larger is harder
    }

# Power Tower 
    buy_pows =  DecimalParameter(0, 2, decimals=3,     default= 0.9967, space="buy", optimize=True)
    sell_pows = DecimalParameter(0.5, 2.5, decimals=3, default= 1.0024, space="sell", optimize=True)
#ClucHAnix
    # buy params
    bbdelta_close = RealParameter(0.0005, 0.02, default=0.01965, space='buy', optimize=True)
    closedelta_close = RealParameter(0.0005, 0.02, default=0.00556, space='buy', optimize=True)
    bbdelta_tail = RealParameter(0.7, 1.0, default=0.95089, space='buy', optimize=True)
    close_bblower = RealParameter(0.0005, 0.02, default=0.00799, space='buy', optimize=True)
    sell_fisher = RealParameter(0.1, 0.5, default=0.38414, space='sell', optimize=True)
    sell_bbmiddle_close = RealParameter(0.97, 1.1, default=1.07634, space='sell', optimize=True)
#BinHV45
    buy_bbdelta = IntParameter(low=1, high=15, default=30, space='buy', optimize=True)
    buy_closedelta = IntParameter(low=15, high=20, default=30, space='buy', optimize=True)
    buy_tail = IntParameter(low=20, high=30, default=30, space='buy', optimize=True)

#-------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------
    @property
    def protections(self):
        return  [
            {
                "method": "CooldownPeriod",
                "stop_duration_candles": 3                     #Legends say no one can get higher than 8 candle
            },
            {
                "method": "StoplossGuard",
                "lookback_period_candles": 14,
                "trade_limit": 1,
                "stop_duration_candles": 8,
                "required_profit": 0.0,
                "only_per_pair": False,
                "only_per_side": False
            },
        ]

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        return dataframe
        
    def populate_entry_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        return dataframe
        
    def populate_exit_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        return dataframe
