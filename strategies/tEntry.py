# --- Do not remove these libs ---
import freqtrade.vendor.qtpylib.indicators as qtpylib

from pandas import DataFrame
from tIndy import tIndy
#------------------------------------------------------------------------------------------------

class tEntry(tIndy):
    def global_check_is_entry(self, dataframe):
        return (
        # Rsi
            (dataframe['rsi_5m']  <   79)
            &(dataframe['rsi_5m']  >   15)
            & (dataframe['rsi']     <   74)
        # Cci
            & (dataframe['cci_5m']          <  445)
            & (dataframe['cci']             <  400)
        # Cmf
            & (dataframe['cmf_5m'] <  0.40)
            & (dataframe['cmf']    <  0.35)
        # skip pumb time
            & ~(dataframe['open_5m']          * 1.006 <  dataframe['close_5m'])
            & ~(dataframe['open_5m'].shift(1) * 1.008 <  dataframe['close_5m'].shift(1))
            & ~(dataframe['open_5m'].shift(2) * 1.008 <  dataframe['close_5m'].shift(2))
#
            & ~(dataframe['open']          * 1.005 <  dataframe['close'])
            & ~(dataframe['open'].shift(1) * 1.007 <  dataframe['close'].shift(1))
            & ~(dataframe['open'].shift(2) * 1.007 <  dataframe['close'].shift(2))
        # skip dump time
            & ~(dataframe['open_5m']          >  dataframe['close_5m']          * 1.006)
            & ~(dataframe['open_5m'].shift(1) >  dataframe['close_5m'].shift(1) * 1.008)
            & ~(dataframe['open_5m'].shift(2) >  dataframe['close_5m'].shift(2) * 1.008)

            & ~(dataframe['open']          >  dataframe['close']          * 1.005)
            & ~(dataframe['open'].shift(1) >  dataframe['close'].shift(1) * 1.007)
            & ~(dataframe['open'].shift(2) >  dataframe['close'].shift(2) * 1.007)
        # Dead volume
          #  & (dataframe['minv'] > 1)
        )
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
    def populate_entry_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
#---------------------------------------------------------------------------------------------
#01 VWap band 3m
        dataframe.loc[
            (  # strategy Vwap band
                (dataframe['close'] < dataframe['vwap_low'] * 1.00095) #1.0005
                & (dataframe['tcp_percent_4'] > 0.000625) #default 0.04 0.013 0.00663
                & (dataframe['cti'] < -0.01) # -0.34
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#01 VWap band strategy             ' + self.timeVer)
#--------------------------------------------------------------------------------------------------------------------
#02 Power Tower 9m
        dataframe.loc[
            (
                (dataframe['close']          > dataframe['close'].shift(2) ** self.buy_pows.value) &
                (dataframe['close'].shift(1) > dataframe['close'].shift(3) ** self.buy_pows.value) &
                (dataframe['close'].shift(2) > dataframe['close'].shift(4) ** self.buy_pows.value)
            )
            & (dataframe['rsi']     <   50)
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#02# Power Tower Strategy          ' + self.timeVer)
#--------------------------------------------------------------------------------------------------------------------
#03 Reinforced Quickie ------------------------------------------
        dataframe.loc[
            ( 
                # simple v bottom shape (lopsided to the left to increase reactivity)
                # which has to be below a very slow average
                # this pattern only catches a few, but normally very good buy points
                    (  dataframe['avgprice'].shift(5) > dataframe['avgprice'].shift(4))
                    & (dataframe['avgprice'].shift(4) > dataframe['avgprice'].shift(3))
                    & (dataframe['avgprice'].shift(3) > dataframe['avgprice'].shift(2))
                    & (dataframe['avgprice'].shift(2) > dataframe['avgprice'].shift(1))
                    & (dataframe['avgprice'].shift(1) < dataframe['avgprice'])
               #     & (dataframe['low'] < dataframe['bb_middleband'])
              #      & (dataframe['cci'] < -60) #-100
             #       & (dataframe['rsi'] < 42) #030
                  #  & (dataframe['mfi'] < 21) #30
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#03# Reinforced Quickie            ' + self.timeVer)
#--------------------------------------------------------------------------------------------------------------------
#04 Strategy BinHV45
        dataframe.loc[
            (
                (dataframe['bb_delta'] > dataframe['close'] * 0.0029) #0.008 0.0008                          larger is harder 2
                & (dataframe['closedelta'] > dataframe['close'] * 0.0024) #0.0175 0.00175 0.0013             larger is harder 3
                & (dataframe['bb_tail'] < dataframe['bb_delta'] * 0.2900) #0.25      0.8827                  smaller is harder 1
#                dataframe['close'].lt(dataframe['bb_lowerband']) &
                & (dataframe['close'] < dataframe['close'].shift(1))
            ) # Insurance
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#04 Strategy BinHV45               ' + self.timeVer)
#--------------------------------------------------------------------------------------------------------------------
#05 Strategy 002 CLD Hammer###------------------------------------
        dataframe.loc[
            (
                (dataframe['rsi'] <   30) & #25 50
                #(dataframe['cci'] <   50) & #-140
                #(dataframe['bb_lowerband'] > dataframe['close']) &
                (dataframe['CDLHAMMER'] == 100)
            ) # Insurance
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#05# Strategy 002 CLD Hammer       ' + self.timeVer)
#--------------------------------------------------------------------------------------------------------------------
#06 Trend Following Strategy---------------------------------------   
        dataframe.loc[
            ( 
                (dataframe['close'] > dataframe['trend'] * 1.00028) #1.0002                            larger is harder
                & (dataframe['close'].shift(1) * 1.0002 < dataframe['trend'].shift(1))                # larger is harder
                & (dataframe['obv'] > dataframe['obv'].shift(1))
                & (dataframe['rsi'] < 50)
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#06 Trend Following Strategy       ' + self.timeVer)
#---------------------------------------------------------------------------------------------------------------------
#07 Bollinger Strategy--------------------------------------------
        dataframe.loc[
            ( 
            # lowc cross lowerband
                (dataframe['close'] * 1.0002 < dataframe['bb_lowerband'])
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#07 Bollinger Strategy             ' + self.timeVer)
#--------------------------------------------------------------------------------------------------------------------
#08 RSI-------------------------------------   
        dataframe.loc[
            ( 
                    (dataframe['rsi'] < 45) #30 40 47
                    & (dataframe['rsi_5m'] < 62) #45
            ) #& (dataframe['cci'] < -10)
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#08 Dao chieu RSI                  ' + self.timeVer)
#--------------------------------------------------------------------------------------------------------------------
#09 CCI------------------------------------- 
        dataframe.loc[
            (   
                (
                    (dataframe['cci'] < -27) #-270 -150 -100 -55 -18
                    & (dataframe['cci_5m'] < 25) #40 70
                )
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#09 Dao chieu CCI                  ' + self.timeVer) 
#--------------------------------------------------------------------------------------------------------------------
#10 Cmf ------------------------------------ -----------------------------
        dataframe.loc[
            ( 
                (
                    (dataframe['cmf'] < -0.135)  #-0.355 -0.205
                    & (dataframe['cmf_5m'] < -0.02) #-0.1 -0.0225 -0.065
                )
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#10 Đảo chiều CMF                  ' + self.timeVer)
#--------------------------------------------------------------------------------------------------------------------
#12 Scalping----------------------------------------------------------------------------------------------------------
        dataframe.loc[
            (
                (dataframe['adx'] > 21) #30                                          larger is harder
                & (
                    (dataframe['fastk'] < 57) & #30                                  smaller is harder
                    (dataframe['fastd'] < 57) & #30                                  smaller is harder
                    (qtpylib.crossed_above(dataframe['fastk'], dataframe['fastd']))
                )
            )
        # Insurance
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#12 Scalping                       ' + self.timeVer)
#---------------------------------------------------------------------------------------------
#13 Star rise
        dataframe.loc[
            (  #Star Rise
                # Dip check
                    (dataframe['close'] < dataframe['mama']) &
                    (dataframe['r_14'] < -30) &
                    (dataframe['cti'] < 3.0) &
                    (dataframe['adx'] > 10) &     #24 7 
                # Bull confirm
                    (dataframe['mama'] > dataframe['fama'])
                   # & (dataframe['sma50'] > dataframe['sma200'] * 1.01)
                # Overpump check
                    & (dataframe['rsi_84']  < 75) #80
                    & (dataframe['rsi_112'] < 77) #80
                #& (dataframe['close'].rolling(288).max() >= (dataframe['close'] * 1.03 ))
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#13 Star rise                      ' + self.timeVer)
#------------------------------------------------------------------------------------------------------
#14 Adaptive
        dataframe.loc[
            (  #Strategy Adaptive
                (dataframe['kama'] > dataframe['fama']) &
                (dataframe['fama'] > dataframe['mama'] * 0.982) #0.981
                & (dataframe['r_14'] < -27) #-61.3
                & (dataframe['mama_diff'] < -0.0015) # -0.025 -0.005 -0.0014
                & (dataframe['cti'] < -0.12) #-0.715
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#14 Adaptive                       ' + self.timeVer)
#------------------------------------------------------------------------------------------------------
#15 Ichimoku
        dataframe.loc[
            (
                (dataframe['close'] > dataframe['senkou_a'] * 1.0001) &
                (dataframe['close'] > dataframe['senkou_b'] * 1.0001) &
                (dataframe['cloud_red'] == True)
            )
            & (dataframe['rsi']     <   50)
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#15 Ichimoku                       ' + self.timeVer)
#------------------------------------------------------------------------------------------------------
#16 Stra 004
        dataframe.loc[
            (
                (
                    (dataframe['adx'] > 22) | #50
                    (dataframe['slowadx'] > 26) #26
                )
                & (dataframe['fastk'] > dataframe['fastd'])

                & (dataframe['cci'] < 90) # <-50
                & (
                    (dataframe['fastk'].shift(1) < 60) #20
                    & (dataframe['fastd'].shift(1) < 60) #20
                )
                & (
                    (dataframe['slowfastk'].shift(1) < 60) & #30
                    (dataframe['slowfastd'].shift(1) < 60) #30
                )
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#16 Strategy 004                   ' + self.timeVer)
#---------------------------------------------------------------------------------------------------------------------
#17 ADX momentum 1h + #23 AwesomeMacd 1h
        dataframe.loc[
            (
                    (dataframe['adx'] > 17) & #25 #15
                    (dataframe['mom'] > 0.0035) &
                    (dataframe['plus_di'] > 25) &
                    (dataframe['plus_di'] > dataframe['minus_di'])
                    #& (dataframe['macd'] > 0)
                    & (dataframe['ao'] > 0)
                    #& (dataframe['ao'].shift(1) < 0)
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#17 ADX momentum + AwesomeMacd     ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#18 CMC Winner-------------------------------------------------------------------
        dataframe.loc[
            (
                (dataframe['cci'] < 25) #-100 -30  -15 0 10           smaller is harder
                & (dataframe['mfi'] <  43) #0-100               smaller is harder
                & (dataframe['cmo'] <  -1)   #-100 - 100              smaller is harder
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#18 CMC Winner                     ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#21 AdxSmas
        dataframe.loc[
            (
                (dataframe['adx'] > 27) & #25                                        larger is harder
                (qtpylib.crossed_above(dataframe['sma_short'], dataframe['sma_long']))
            )
            & (dataframe['enter_tag'] == '')
            ,
            ['enter_long', 'enter_tag']] = (1, '#21 AdxSmas                        ' + self.timeVer)


#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
# Check all
        dataframe.loc[
        # Dont enter if not stable
            (self.global_check_is_entry(dataframe) == False)
            ,
            ['enter_long', 'enter_tag']] = (0, '')
#------------------------------------------------------------------------------------------------------
        return dataframe
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 