# --- Do not remove these libs ---
import freqtrade.vendor.qtpylib.indicators as qtpylib

from pandas import DataFrame
from tEntry import tEntry
# --------------------------------

def is_empty_tag(dataframe):
    return (
        (dataframe['exit_tag'] == '')
    )  

class tExit(tEntry):
    def populate_exit_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:

#-----------------------------------------------------------------------------
#01 Power Tower
        dataframe.loc[
            (
                (dataframe['close']            < dataframe['close'].shift(2) ** self.sell_pows.value) |
                (dataframe['close'].shift(1) < dataframe['close'].shift(3) ** self.sell_pows.value) |
                (dataframe['close'].shift(2) < dataframe['close'].shift(4) ** self.sell_pows.value)
            ) & is_empty_tag(dataframe) ,
            ['exit_long', 'exit_tag']] = (1, '#01# Power Tower Strategy   ' + self.timeVer)
#-----------------------------------------------------------------------------------------------------------
#02 Đảo chiều RSI ####
        dataframe.loc[
            ( 
                (
                        (dataframe['rsi_5m'] > 70) #Check rsi BTC #70
                        | (dataframe['rsi'] > 75) #75
                ) |
                (
                        (dataframe['rsi'] < 25) #25
                        | (dataframe['rsi_5m'] < 30) #Check rsi BTC
                )
            ) & is_empty_tag(dataframe) 
            ,
            ['exit_long', 'exit_tag']] = (1, '#02 Dao chieu RSI           ' + self.timeVer)
#-------------------------------------------------------------------------------------------------------------------------------------
#03 Đảo chiều CCI ####
        dataframe.loc[
            ( 
                (
                    (
                        (dataframe['cci_5m'] > 190) | # Check cci BTC #180 160 155
                        (dataframe['cci'] > 230) #180
                    ) #& (dataframe['cmf'] > 0.23)
                ) |
                (
                    (
                        (dataframe['cci_5m'] < -190) | # Check cci BTC 
                        (dataframe['cci'] < -230)
                    ) #& (dataframe['cmf'] < -0.23)
                )
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#03 Đảo chiều CCI           ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#04 Đảo chiều CMF ####
        dataframe.loc[
            ( 
                (
                    (
                        (dataframe['cmf'] < -0.24) #-0.27 0.22
                        |(dataframe['cmf_5m'] < -0.21) #Check BTC #-0.27
                    ) #& (dataframe['cci'] < 0)
                ) |
                (
                    (
                        (dataframe['cmf'] > 0.24) #0.27
                        | (dataframe['cmf_5m'] > 0.21) #Check BTC #0.27
                    ) #& (dataframe['cci'] > 0)
                )
            ) & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#04 Dao chieu CMF           ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#05# Always sell on 8 green candles
        dataframe.loc[
            (  
                    (dataframe['open'] * 1.0005 < dataframe['close']) & #Green
                    (dataframe['open'].shift(1) * 1.0002 < dataframe['close'].shift(1)) &    #Small green
                    (dataframe['open'].shift(2) * 1.0002 < dataframe['close'].shift(2)) &    #Small green
                    (dataframe['open'].shift(3) < dataframe['close'].shift(3) * 1.0002) &    #Small red
                    (dataframe['open'].shift(4) < dataframe['close'].shift(4) * 1.0002) &    #Small red
                    (dataframe['open'].shift(5) < dataframe['close'].shift(5) * 1.0002) &    #Small red
                    (dataframe['open'].shift(6) < dataframe['close'].shift(6) * 1.0002) &    #Small red
                    (dataframe['open'].shift(7) < dataframe['close'].shift(7) * 1.0002)      #Small red
            ) & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#05# 8 Green candles        ' + self.timeVer) 
# ------------------------------------------------------------------------------------------------------------------------------------
#06 Strategy 002 CLD Hammer-------------------------------------------------
        dataframe.loc[
            ( 
                (
                    (dataframe['sar'] > dataframe['close'])
                    & (dataframe['fisher_rsi'] > 0.30) #0.30
                #    & (dataframe['rsi'] > 40)
                #    & (dataframe['cci'] > 50) #60
                )
				| (dataframe['CDLHAMMER'] == -100)
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#06 Strategy 002 CLD Hammer ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#07 MACD Cross 5m -----------------------------------------------
        dataframe.loc[
            (  
                (dataframe['macd'] < dataframe['macdsignal'])
          #      & (dataframe['macd'].shift(1) > dataframe['macdsignal'].shift(1))
                # Insurance (default 0.4)
                #& (dataframe['cmf'] > -0.065) 
            ) & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#07 MACD Cross              ' + self.timeVer) 
# ------------------------------------------------------------------------------------------------------------------------------------
#09 Bollingerband------------------------------------ 
        dataframe.loc[
            ( 
            # Expansion zone
                (dataframe['bb_lowerband'] * 1.017 < dataframe['bb_upperband'] ) #1.016 1.024
                #| (dataframe['bb_lowerband_5m'].shift(1) * 1.010 < dataframe['bb_upperband_5m'].shift(1) ) 
            # high cross upperband
                & (dataframe['high'] > dataframe['bb_upperband'] * 0.997) #1
            )
            & is_empty_tag(dataframe) 
            ,
            ['exit_long', 'exit_tag']] = (1, '#09 Bollingerband           ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#10 Ichimoku------------------------------------ 
        dataframe.loc[
            ( 
                (dataframe['close'] > dataframe['kijun']) &
                (
                    (dataframe['close']  <  dataframe['senkou_a'])
                    | (dataframe['close']  <  dataframe['senkou_b'])
                )
                & (dataframe['cloud_green'] == True)
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#10# Ichimoku               ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#12 CME gap-------------------------------------------------------
        dataframe.loc[
            (
                (dataframe['lowc'] * 1.0037 < dataframe['highc']) #Green candle 2% #1.0038
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#12 CME gap                 ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#13 Strategy 004-------------------------------------------------------
        dataframe.loc[
            (
                (dataframe['slowadx'] < 25) &
                (
                    (dataframe['fastk'] > 70) 
                    & (dataframe['fastd'] > 70)
                ) &
                (dataframe['fastk'] < dataframe['fastd']) &
                (dataframe['close'] > dataframe['ema7'])
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#13 Strategy 004            ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#14 Strategy 001-------------------------------------------------------
        dataframe.loc[
            (
            #    qtpylib.crossed_above(dataframe['ema50'], dataframe['ema100']) &
              #  (dataframe['ha_close'] < dataframe['ema25']) &
                (dataframe['ha_open'] > dataframe['ha_close'])  # red bar
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#14 Strategy 001            ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#15 Adx Momentum 1h-------------------------------------------------------
        dataframe.loc[
            (
                    (dataframe['adx'] > 25)
                    & (dataframe['mom'] < 0)
                #    (dataframe['minus_di'] > 25)
                 #   & (dataframe['plus_di'] < dataframe['minus_di'])
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#15 Adx Momentum 1h         ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------
#16 CMC Winner-------------------------------------------------------------------
        dataframe.loc[
            (
                (dataframe['cci']   > 100)   #100
                & (dataframe['mfi'] > 65)  #80 30
                & (dataframe['cmo'] > 30)    #50 20
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#16 CMC Winner              ' + self.timeVer)  
# ------------------------------------------------------------------------------------------------------------------------------------
#17 Quickies 5m-------------------------------------------------------------------
        dataframe.loc[
            (
                    (dataframe['adx'] > 30) & #70
                    (dataframe['tema'] > dataframe['bb_middleband'])
               #     (dataframe['tema'] < dataframe['tema'].shift(1))
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#17 Quickies 5m             ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------            
#18 BinHV27-------------------------------------------------------------------
        dataframe.loc[
            ((
                #(~dataframe['preparechangetrend'])
                #& 
                ( ~dataframe['continueup'])
                # & (~dataframe['bigup'])
            )
            | (
                (dataframe['emarsi'] > 75)
                & dataframe['slowingdown']
            )
            | (
                (dataframe['minusdi'] > dataframe['plusdi'])
                & (dataframe['close'] > dataframe['lowema'])
            ))
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#18 BinHV27                 ' + self.timeVer)
# ------------------------------------------------------------------------------------------------------------------------------------            
#19 Stra 002 CLDHammer-------------------------------------------------------------------
        dataframe.loc[
            (
                (dataframe['sar'] > dataframe['close']) &
                (dataframe['fisher_rsi'] > 0.3)
            )
            & is_empty_tag(dataframe)
            ,
            ['exit_long', 'exit_tag']] = (1, '#19 Stra002 CLDHammer       ' + self.timeVer)



            
            
            
            
        return dataframe
