class tEntry(tIndy):
    def global_check_is_dump(self, dataframe):
        return (
            (   
                (dataframe['resample_5_low'] < dataframe['resample_5_min'] * 1.0001) # Do not buy when market dump
                | (dataframe['low_3m'] < dataframe['min_3m'] * 1.0001) # Do not buy when market dump
            ) | ( 
                (dataframe['resample_5_lowc'] * 1.015 > dataframe['resample_5_highc']) # Do not buy when market dump 
                | (dataframe['lowc_3m'] * 1.015 > dataframe['highc_3m']) # Do not buy when market dump 
            )
        )