# --- Do not remove these libs ---
import freqtrade.vendor.qtpylib.indicators as qtpylib
import numpy as np
import pandas_ta as pta
import talib.abstract as ta

from freqtrade.strategy import merge_informative_pair
from pandas import DataFrame, Series, DatetimeIndex, merge
from technical.indicators import ichimoku
from technical.util import resample_to_interval, resampled_merge

from tBuySell import tBuySell
# --------------------------------

# VWAP bands
def VWAPB(dataframe, window_size=20, num_of_std=1):
    df = dataframe.copy()
    df['vwap'] = qtpylib.rolling_vwap(df,window=window_size)
    rolling_std = df['vwap'].rolling(window=window_size).std()
    df['vwap_low'] = df['vwap'] - (rolling_std * num_of_std)
    df['vwap_high'] = df['vwap'] + (rolling_std * num_of_std)
    return df['vwap_low'], df['vwap'], df['vwap_high']

# Williams %R
def williams_r(dataframe: DataFrame, period: int = 14) -> Series:
    """Williams %R, or just %R, is a technical analysis oscillator showing the current closing price in relation to the high and low
        of the past N days (for a given N). It was developed by a publisher and promoter of trading materials, Larry Williams.
        Its purpose is to tell whether a stock or commodity market is trading near the high or the low, or somewhere in between,
        of its recent trading range.
        The oscillator is on a negative scale, from −100 (lowest) up to 0 (highest).
    """

    highest_high = dataframe["high"].rolling(center=False, window=period).max()
    lowest_low = dataframe["low"].rolling(center=False, window=period).min()

    WR = Series(
        (highest_high - dataframe["close"]) / (highest_high - lowest_low),
        name=f"{period} Williams %R",
        )

    return WR * -100

def top_percent_change(dataframe: DataFrame, length: int) -> float:
    """
    Percentage change of the current close from the range maximum Open price
    :param dataframe: DataFrame The original OHLC dataframe
    :param length: int The length to look back
    """
    if length == 0:
        return (dataframe['open'] - dataframe['close']) / dataframe['close']
    else:
        return (dataframe['open'].rolling(length).max() - dataframe['close']) / dataframe['close']

class tIndy(tBuySell):
    def informative_pairs(self):
        """
        Define additional, informative pair/interval combinations to be cached from the exchange.
        These pair/interval combinations are non-tradeable, unless they are part
        of the whitelist as well.
        For more information, please consult the documentation
        :return: List of tuples in the format (pair, interval)
            Sample: return [("ETH/USDT", "5m"),
                            ("BTC/USDT", "15m"),
                            ]
        """
        return [(f"BTC/USDT", '5m')]

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe['enter_tag'] = ''
        dataframe['exit_tag'] = ''
        dataframe['f_enter'] = False

    # Min max 13 candles
        dataframe['highc'] = dataframe[['close', 'open']].apply(max, axis=1)
        dataframe['lowc'] = dataframe[['close', 'open']].apply(min, axis=1)
        dataframe['max'] = ta.MAX(dataframe['highc'], timeperiod=15)
        dataframe['min'] = ta.MIN(dataframe['lowc'], timeperiod=15)
        dataframe['maxCME'] = np.nan_to_num(ta.MAX(dataframe['highc'], timeperiod=9))
        dataframe['minCME'] = np.nan_to_num(ta.MIN(dataframe['lowc'], timeperiod=9))
        dataframe['minv']   = np.nan_to_num(ta.MIN(dataframe['volume'], timeperiod=5))                                              # Min volume on 4 candle
        dataframe['maxv']   = np.nan_to_num(ta.MAX(dataframe['volume'], timeperiod=3) )                                             # MAX volume on 13 candle
    # Average 13 candles
        avgprice = ((dataframe['lowc'] + dataframe['highc']) / 2)
        dataframe['avgprice'] = avgprice
        dataframe['avgvolume'] = dataframe['volume'].rolling(13).mean()
    # Scalping 1m
        dataframe['ema_low'] = ta.EMA(dataframe, timeperiod=5, price='low')
        dataframe['ema_high'] = ta.EMA(dataframe, timeperiod=7, price='high')
        stoch_fast = ta.STOCHF(dataframe, 5, 3, 0, 3, 0)
        dataframe['fastd'] = stoch_fast['fastd']
        dataframe['fastk'] = stoch_fast['fastk']
        # Slow Stoch
        slowstoch = ta.STOCHF(dataframe, 50)
        dataframe['slowfastd'] = slowstoch['fastd']
        dataframe['slowfastk'] = slowstoch['fastk']
    # ADX
        dataframe['adx'] = ta.ADX(dataframe, timeperiod=14)
        dataframe['slowadx'] = ta.ADX(dataframe, 35)
    # SMA - Simple Moving Average
        dataframe['sma50']  = ta.SMA(dataframe['avgprice'],  timeperiod= 50)
        dataframe['sma200'] = ta.SMA(dataframe['avgprice'],  timeperiod=200)
        dataframe['tema']   = ta.TEMA(dataframe['avgprice'], timeperiod=  9)
        dataframe['highsma'] = np.nan_to_num(ta.EMA(dataframe, timeperiod=120))
        dataframe['lowsma'] = np.nan_to_num(ta.EMA(dataframe, timeperiod=60))

    # CCIStrategy
        dataframe['cmf'] = self.chaikin_mf(dataframe)
        dataframe['cmo'] = ta.CMO(dataframe)
        dataframe['cci'] = ta.CCI(dataframe, timeperiod=25) 
        dataframe['CDLHAMMER'] = np.nan_to_num(ta.CDLHAMMER(dataframe))                                             
    # RSI
        dataframe['rsi']     = ta.RSI(dataframe, timeperiod=14)
        dataframe['rsi_84']  = ta.RSI(dataframe, timeperiod=84)
        dataframe['rsi_112'] = ta.RSI(dataframe, timeperiod=112)
    # Inverse Fisher transform on RSI, values [-1.0, 1.0] (https://goo.gl/2JGGoy)
        rsi_3m = 0.1 * (dataframe['rsi'] - 50)
        dataframe['fisher_rsi'] = (np.exp(2 * rsi_3m) - 1) / (np.exp(2 * rsi_3m) + 1)
    # Ema 3m
        dataframe['ema7'] = ta.EMA(dataframe,   timeperiod=7,   price='highc')
        dataframe['ema25'] = ta.EMA(dataframe,  timeperiod=25,  price='highc')
        dataframe['ema50'] = ta.EMA(dataframe,  timeperiod=50,  price='highc')
        dataframe['ema100'] = ta.EMA(dataframe, timeperiod=100, price='highc')
        dataframe['ema150'] = ta.EMA(dataframe, timeperiod=150, price='highc')
        dataframe['ema420'] = ta.EMA(dataframe, timeperiod=420, price='highc')
    # %R
        dataframe['r_14'] = williams_r(dataframe, period=14)
    # Adaptive
        dataframe['kama'] = ta.KAMA(dataframe['lowc'], 84)
        dataframe['mama'], dataframe['fama'] = ta.MAMA(dataframe['avgprice'], 0.5, 0.05)
        dataframe['mama_diff'] = ( ( dataframe['mama'] - dataframe['fama'] ) / dataframe['avgprice'] )
        dataframe['cti'] = pta.cti(dataframe['lowc'], length=20)
    #Ichimoku
        ichi_3m = ichimoku(dataframe, conversion_line_period=20, base_line_periods=60, laggin_span=120, displacement=30)
        dataframe['tenkan'] = ichi_3m['tenkan_sen']
        dataframe['kijun'] = ichi_3m['kijun_sen']
        dataframe['senkou_a'] = ichi_3m['senkou_span_a']
        dataframe['senkou_b'] = ichi_3m['senkou_span_b']
        dataframe['cloud_green'] = ichi_3m['cloud_green']
        dataframe['cloud_red'] = ichi_3m['cloud_red']
    # Heikinashi
        heikinashi_3m = qtpylib.heikinashi(dataframe)
        dataframe['ha_open'] = heikinashi_3m['open']
        dataframe['ha_close'] = heikinashi_3m['close']
        dataframe['ha_high'] = heikinashi_3m['high']
        dataframe['ha_low'] = heikinashi_3m['low']
    # Bollinger
        bollinger_3m = qtpylib.bollinger_bands(qtpylib.typical_price(dataframe), window=20, stds=2)
        dataframe['bb_lowerband']  = bollinger_3m['lower']
        dataframe['bb_upperband']  = bollinger_3m['upper']
        dataframe['bb_middleband'] = bollinger_3m['mid']
        dataframe['bb_delta']   = (dataframe['bb_middleband'] - dataframe['bb_lowerband']).abs()
        dataframe['closedelta'] = (dataframe['close'] - dataframe['close'].shift(1)).abs()   
        dataframe['bb_tail']    = (dataframe['close'] - dataframe['low']).abs()
    # SAR Parabol
        dataframe['sar'] = ta.SAR(dataframe)
    # Adx momentum 1h
        dataframe['plus_di'] = ta.PLUS_DI(dataframe, timeperiod=25)
        dataframe['minus_di'] = ta.MINUS_DI(dataframe, timeperiod=25)
        dataframe['mom'] = ta.MOM(dataframe, timeperiod=14)
    # Awesome MACD 3m
        dataframe['ao'] = qtpylib.awesome_oscillator(dataframe)
    # MACD
        macd_3m = ta.MACD(dataframe)
        dataframe['macd'] = macd_3m['macd']
        dataframe['macdsignal'] = macd_3m['macdsignal']
        dataframe['macdhist'] = macd_3m['macdhist']
    # MFI
        dataframe['mfi'] = ta.MFI(dataframe)
    #VWAP 3m--------------------------------------------------------
        vwap_low, vwap, vwap_high = VWAPB(dataframe, 20, 1)
        dataframe['vwap_low'] = vwap_low
        dataframe['tcp_percent_4'] = top_percent_change(dataframe, 4)
    # Slow Stoch 3m-------------------------------------------------
        stoch_3m = ta.STOCH(dataframe)
        dataframe['slowk'] = stoch_3m['slowk']
    # Trend following 5m
        dataframe['trend'] = dataframe['highc'].ewm(span=21, adjust=True).mean()
        dataframe['obv'] = ta.OBV(dataframe['highc'], dataframe['volume'])
    # get the rolling volume mean for the last hour (12x5)
        # Note: dataframe['volume'].mean() uses the whole dataframe in 
        # backtesting hence will have lookahead, but would be fine for dry/live use
        dataframe['mean-volume'] = dataframe['volume'].rolling(12).mean()
    # BinHV27
        dataframe['minusdi'] = np.nan_to_num(ta.MINUS_DI(dataframe))
        dataframe['plusdi']  = np.nan_to_num(ta.PLUS_DI(dataframe))
        rsiframe = DataFrame(dataframe['rsi']).rename(columns={'rsi': 'close'})
        dataframe['emarsi'] = np.nan_to_num(ta.EMA(rsiframe, timeperiod=5))
        dataframe['lowema']  = np.nan_to_num(ta.EMA(dataframe, timeperiod=60))
    #AdxSmas
        dataframe['sma_short'] = ta.SMA(dataframe, timeperiod=3)
        dataframe['sma_long'] = ta.SMA(dataframe, timeperiod=6)

# resample our dataframes tf = 3m ---------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------

#Informative pair 3m
# ------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------
        if self.dp:
            # Get ohlcv data for informative pair at 3m interval.
            inf_tf = '5m'
            informative = self.dp.get_pair_dataframe(pair=f"BTC/USDT", timeframe=inf_tf)
        # Min max 13 candles
            informative['highc'] = informative[['close', 'open']].apply(max, axis=1)
            informative['lowc'] = informative[['close', 'open']].apply(min, axis=1)
            informative['max'] = ta.MAX(informative, timeperiod=15, price='highc') 
            informative['min'] = ta.MIN(informative, timeperiod=15, price='lowc')
            avgprice_inf = ((informative['lowc'] + informative['highc']) / 2)
            informative['avgprice'] = avgprice_inf
        # Ema------------------------------------------------------------------
            informative['ema100'] = ta.EMA(informative, timeperiod=100, price='lowc')
        # calculate rsi, cci on informative pair
            informative['rsi'] = ta.RSI(informative, timeperiod=14)
            informative['cci'] = ta.CCI(informative, timeperiod=25)
            informative['cmf'] = self.chaikin_mf(informative)
        # Bollingerband
            bollinger_inf = qtpylib.bollinger_bands(qtpylib.typical_price(informative), window=20, stds=2)
            informative['bb_lowerband'] = bollinger_inf['lower']
            informative['bb_upperband'] = bollinger_inf['upper']
            informative['bb_middleband'] = bollinger_inf['mid']

    # Combine the 2 dataframe
    # This will result in a column named 'closeETH' or 'closeBTC' - depending on stake_currency.
        dataframe = merge_informative_pair(dataframe, informative, self.timeframe, inf_tf, ffill=True)
# ------------------------------------------------------------------------------------------------------------------------------------
    # Drop unused column
        drop_columns = [
                        'maxv',
                        'maxCME_5m','minCME_5m','maxv_5m','avgprice_5m','avgvolume_5m',

                        'ema_high','sma200',
                        'ema_low_5m','ema_high_5m','fastd_5m','fastk_5m','slowfastd_5m','slowfastk_5m','adx_5m','slowadx_5m','sma50_5m','sma200_5m','tema_5m','cmo_5m',

                        'ema25','ema50','ema100','ema150','ema420',
                        'rsi_84_5m','rsi_112_5m','fisher_rsi_5m','ema7_5m','ema25_5m','ema50_5m','ema100_5m','ema150_5m','ema420_5m','r_14_5m',

                        'tenkan',
                        'kama_5m','mama_5m','mama_diff_5m','cti_5m','tenkan_5m','kijun_5m','senkou_a_5m','senkou_b_5m','cloud_green_5m','cloud_red_5m',

                        'ha_high','ha_low',
                        'ha_open_5m','ha_close_5m','ha_high_5m','ha_low_5m','bb_lowerband_5m','bb_upperband_5m','bb_middleband_5m','bb_delta_5m','closedelta_5m','bb_tail_5m',

                        'macdhist','CLDHAMMER',
                        'sar_5m','plus_di_5m','minus_di_5m','mom_5m','ao_5m','macd_5m','macdsignal_5m','macdhist_5m','mfi_5m','vwap_low_5m','tcp_percent_4_5m','CLDHAMMER_5m',

                        'slowk','mean-volume','fastema','slowema','delta',
                        'slowk_5m','trend_5m','obv_5m','mean-volume_5m','fastema_5m','slowema_5m','lowema_5m','highema_5m','continueup_5m','emarsi_5m','delta_5m','slowingdown_5m',

                        ]
        dataframe.drop(columns=dataframe.columns.intersection(drop_columns), inplace=True)
# ------------------------------------------------------------------------------------------------------------------------------------

        return dataframe
# ------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------

    def chaikin_mf(self, df, periods=25):
        close = df['close']
        low = df['low']     
        high = df['high']
        volume = df['volume']

        mfv = ((close - low) - (high - close)) / (high - low)
        mfv = mfv.fillna(0.0)  # float division by zero
        mfv *= volume
        cmf = mfv.rolling(periods).sum() / volume.rolling(periods).sum()

        return Series(cmf, name='cmf')

    def resample(self, dataframe, interval, factor):
        # defines the reinforcement logic
        # resampled dataframe to establish if we are in an uptrend, downtrend or sideways trend
        df = dataframe.copy()
        df = df.set_index(DatetimeIndex(df['date']))
        ohlc_dict = {
            'open': 'first',
            'high': 'max',
            'low': 'min',
            'close': 'last',
        }
        df = df.resample(str(int(interval[:-1]) * factor) + 'min', label="right").agg(ohlc_dict)
        df['resample_sma'] = ta.SMA(df, timeperiod=100, price='close')
        df['resample_medium'] = ta.SMA(df, timeperiod=50, price='close')
        df['resample_short'] = ta.SMA(df, timeperiod=25, price='close')
        df['resample_long'] = ta.SMA(df, timeperiod=200, price='close')
        df = df.drop(columns=['open', 'high', 'low', 'close'])
        df = df.resample(interval[:-1] + 'min')
        df = df.interpolate(method='time')
        df['date'] = df.index
        df.index = range(len(df))
        dataframe = merge(dataframe, df, on='date', how='left')
        return dataframe

    def get_ticker_indicator(self):
        return int(self.timeframe[:-1])












